import json
with open("Futbol.json") as fichero:
    datos=json.load(fichero)

#Funcion 1

def listar_informacion(datos):
    lista=[]
    for nombres in datos["rounds"]:
        for equipos in nombres["matches"]:
            lista.append(equipos["team1"]["name"])
            lista.append(equipos["team2"]["name"])
        return lista
        
            
#Función 2

def contar_informacion(datos):
    lista=[]
    print(datos["name"])
    return len(datos["rounds"])

#Función 3

def buscar_informacion(datos,fecha):
    lista=[]
    indicador=False
    for rondas in datos["rounds"]:
        for partidos in rondas["matches"]:
            if fecha==partidos["date"]:
                lista.append(partidos["team1"]["name"])
                lista.append(partidos["team2"]["name"])
                indicador=True
    if indicador==False:
        print("En la fecha indicada no hay partidos")
    return lista

#Funcion 4

def filtrar_informacion(datos,equipo):
    lista=[]
    for rondas in datos["rounds"]:
        for partido in rondas["matches"]:
            if equipo==partido["team1"]["name"] or equipo==partido["team2"]["name"]:

                lista.append(partido["date"])
    return lista

#Funcion 5

def ejercicio_libre(datos,jornada):
    lista1=[]
    for jornadas in datos["rounds"]:
        if jornada==jornadas["name"]:
            for equipos in jornadas["matches"]:
                lista=[]
                lista.append(equipos["team1"]["name"])
                lista.append("vs")
                lista.append(equipos["team2"]["name"])
                lista1.append(lista)
    return lista1

