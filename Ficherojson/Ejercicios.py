from Funciones import listar_informacion,contar_informacion,buscar_informacion,filtrar_informacion,ejercicio_libre
import json
with open("Futbol.json") as fichero:
    datos=json.load(fichero)

print('''Menú
Lista primero todos los equipos para ver los nombres.

1. Listar Información (Lista los equipos de la liga)
2. Contar información (Mostrar el nombre de la liga y cuantas jornadas tiene)
3. Buscar información (Mostrar los equipos que van a jugar en una fecha dada.)
4. Filtrar información (Pedir por teclado el nombre de un equipo y mostrar las fechas en las que va a jugar.)
5. Ejercicio libre (Muestra todos los enfrentamientos de una jornada concreta.)
0. Salir
''')

opcion=int(input("Introduzca la opción que desea escoger:"))

while opcion!=0:

    if opcion<0 or opcion>5:
        print("Error, opción inválida")
    
    
    if opcion==1:

#Ejercicio 1

        
        print("\n".join (listar_informacion(datos)))

#Ejercicio 2
    if opcion==2:

        print(contar_informacion(datos))

#Ejercicio 3
    if opcion==3:

        fecha1=input("Introduzca una fecha(Formato YYYY-MM-DD):")
        print("\n".join (buscar_informacion(datos,fecha1)))

#Ejercicio 4
    if opcion==4:

        equipo1=input("Introduzca el nombre de un equipo:")
        print("\n".join(filtrar_informacion(datos,equipo1)))

#Ejercicio 5
    if opcion==5:
        jornada2=input("Introduzca la jornada(Jornada 3, Jornada 28 ...):")
        for x in ejercicio_libre(datos,jornada2):
            print(" ".join(x))
    opcion=int(input("Introduzca la opción que desea escoger:"))
print("Fin del programa")