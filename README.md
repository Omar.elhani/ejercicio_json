# Ejercicio_Json

**EJERCICIO JSON**

A Partir del fichero JSON Futbol.json obtener la siguiente información:

1.**Listar Información**: Lista los equipos de la liga.

2.**Contar Información**: Mostrar el nombre de la liga y cuantas jornadas tiene.

3.**Buscar o filtrar información**: Mostrar los equipos que van a jugar en una fecha dada.

4.**Buscar información relacionada**: Pedir por teclado el nombre de un equipo y mostrar las fechas en las que va a jugar.

5.**Ejercicio libre**: Muestra todos los enfrentamientos de una jornada concreta.

 
